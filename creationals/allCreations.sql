-- 1
CREATE TABLE Country (
  country_name name_domain PRIMARY KEY ,
  continent VARCHAR(10) NOT NULL,
  CHECK (continent = 'asia' or continent = 'europe' or continent = 'africa' or continent = 'australia' or continent = 'america'),
  geo_location VARCHAR(20),
  flag_path VARCHAR(100),
  language VARCHAR(10) DEFAULT 'english',
  CHECK (language = 'english' or language = 'farsi')
);
-- 2
CREATE TABLE AbstractUser (
  username name_domain PRIMARY KEY,
  email VARCHAR(100) not NULL UNIQUE,
  password VARCHAR(30) not NULL,
  isAdmin BOOLEAN not NULL DEFAULT FALSE
);
-- 3
CREATE TABLE UserDetails (
  username                name_domain PRIMARY KEY REFERENCES AbstractUser(username),
  premium_expiration_date TIMESTAMP,
  postal_code             VARCHAR(30),
  view_details            INTEGER,
  CHECK (view_details = 1 OR view_details = 2 OR view_details = 3),
  country_name            name_domain REFERENCES Country(country_name)
);
-- 4
CREATE TABLE Transaction (
  t_id VARCHAR(30) PRIMARY KEY ,
  gateway VARCHAR(20),
  CHECK (gateway = 'saman' or gateway = 'shaparak'),
  amount INTEGER NOT NULL,
  t_time TIMESTAMP NOT NULL DEFAULT now(),
  account_number VARCHAR(20),
  username name_domain REFERENCES abstractuser(username)
);
-- 5
CREATE TABLE UserFollow (
  follower_username  name_domain REFERENCES AbstractUser(username),
  following_username name_domain REFERENCES AbstractUser(username),
  accepted BOOLEAN,
  PRIMARY KEY (follower_username, following_username)
);
-- 6
CREATE TABLE Followable(
  f_id INTEGER PRIMARY KEY ,
  type VARCHAR(10),
  CHECK (type = 'artist' or type = 'playlist' or type = 'radio')
);
-- 7
CREATE TABLE Album (
  album_name name_domain PRIMARY KEY ,
  year INTEGER,
  CHECK (year > 1900 and year < 2018),
  studio VARCHAR(20),
  total_plays INTEGER,
  CHECK (total_plays >= 0),
  total_downloads INTEGER,
  CHECK (total_downloads >= 0),
  cover_photo_path address_domain
);
-- 8
CREATE TABLE Artist (
  artist_name name_domain PRIMARY KEY ,
  verified BOOLEAN NOT NULL DEFAULT FALSE ,
  f_id BIGINT REFERENCES followable(f_id),
  photo_path address_domain,
  UNIQUE (f_id)
);
-- 9
CREATE TABLE AlbumArtist (
  artist_name name_domain REFERENCES Artist(artist_name),
  album_name name_domain REFERENCES Album(album_name),
  PRIMARY KEY (artist_name, album_name)
);
-- 10
CREATE TABLE Song(
  album_name name_domain REFERENCES album(album_name),
  song_name name_domain,
  total_plays INTEGER DEFAULT 0,
  CHECK (total_plays >= 0),
  total_downloads INTEGER DEFAULT 0,
  CHECK (total_downloads >= 0),
  song_path address_domain UNIQUE ,
  PRIMARY KEY (album_name, song_name)
);
-- 11
CREATE TABLE SongListen(
  album_name name_domain,
  song_name name_domain,
  day TIMESTAMP not NULL DEFAULT now(),
  count INTEGER DEFAULT 0 NOT NULL,
  CHECK (count > 0),
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name)
);
-- 12
CREATE TABLE Playlist (
  playlist_name name_domain PRIMARY KEY ,
  photo_path address_domain,
  f_id INTEGER REFERENCES followable(f_id),
  UNIQUE (f_id)
);
-- 13
CREATE TABLE PlaylistSongs (
  playlist_name name_domain REFERENCES Playlist (playlist_name),
  album_name    name_domain,
  song_name     name_domain,
  added_time    TIMESTAMP NOT NULL DEFAULT now(),
  FOREIGN KEY (album_name, song_name) REFERENCES song (album_name, song_name),
  PRIMARY KEY (playlist_name, album_name, song_name)
);
-- 14
CREATE TABLE Radio(
  radio_name name_domain PRIMARY KEY ,
  country_name name_domain  REFERENCES country(country_name),
  photo_path address_domain,
  f_id INTEGER REFERENCES followable(f_id),
  UNIQUE (f_id)
);
-- 15
CREATE TABLE RadioSongs (
  radio_name name_domain REFERENCES Radio (radio_name),
  album_name    name_domain,
  song_name     name_domain,
  added_time    TIMESTAMP NOT NULL DEFAULT now(),
  FOREIGN KEY (album_name, song_name) REFERENCES song (album_name, song_name),
  PRIMARY KEY (radio_name, album_name, song_name)
);
-- 16
CREATE TABLE UserListen (
  username name_domain REFERENCES abstractuser(username),
  album_name name_domain,
  song_name name_domain,
  day TIMESTAMP not NULL DEFAULT now(),
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name)
);
-- 17
CREATE TABLE Genre(
  genre_name genre_domain PRIMARY KEY ,
  photo_path address_domain
);
-- 18
CREATE TABLE Mood(
  mood_name name_domain PRIMARY KEY ,
  photo_path address_domain
);
-- 19
CREATE TABLE AlbumGenre (
  album_name name_domain REFERENCES album(album_name),
  genre_name genre_domain REFERENCES genre(genre_name),
  PRIMARY KEY (album_name, genre_name)
);
-- 20
CREATE TABLE SongGenre(
  album_name name_domain,
  song_name name_domain,
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name),
  genre_name genre_domain REFERENCES genre(genre_name),
  PRIMARY KEY (album_name,song_name, genre_name)
);
-- 21
CREATE TABLE ArtistGenre (
  artist_name name_domain REFERENCES Artist(artist_name),
  genre_name genre_domain REFERENCES Genre(genre_name),
  PRIMARY KEY (artist_name, genre_name)
);
-- 22
CREATE TABLE RadioGenre (
  radio_name name_domain REFERENCES Radio(radio_name),
  genre_name genre_domain REFERENCES Genre(genre_name),
  PRIMARY KEY (radio_name, genre_name)
);
-- 23
CREATE TABLE RadioMood (
  radio_name name_domain REFERENCES Radio(radio_name),
  mood_name genre_domain REFERENCES Mood(mood_name),
  PRIMARY KEY (radio_name, mood_name)
);
-- 24
CREATE TABLE PlaylistMood (
  playlist_name name_domain REFERENCES Playlist(playlist_name),
  mood_name genre_domain REFERENCES Mood(mood_name),
  PRIMARY KEY (playlist_name, mood_name)
);
-- 25
CREATE TABLE PlaylistGenre (
  playlist_name name_domain REFERENCES Playlist(playlist_name),
  genre_name genre_domain REFERENCES Genre(genre_name),
  PRIMARY KEY (playlist_name, genre_name)
);
-- 26
CREATE TABLE Follow (
  username name_domain REFERENCES AbstractUser(username),
  f_id INTEGER REFERENCES Followable(f_id),
  PRIMARY KEY (username, f_id)
);
-- 27
CREATE TABLE Saved (
  username name_domain REFERENCES AbstractUser(username),
  album_name name_domain,
  song_name name_domain,
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name),
  PRIMARY KEY (username, album_name, song_name)
);
-- 28
CREATE TABLE PlaylistAdmin (
  playlist_name name_domain REFERENCES Playlist(playlist_name),
  username name_domain REFERENCES AbstractUser(username),
  PRIMARY KEY (username, playlist_name)
)