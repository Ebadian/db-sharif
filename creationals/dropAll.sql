DROP VIEW AllUsers;
DROP VIEW AdminUsers;
DROP VIEW UserArtistFollows;
DROP VIEW UserUserAcceptedFollow;
DROP TRIGGER songDownloads ON song;
DROP VIEW easytransaction;
DROP VIEW UserUserAcceptedFollow;

drop table PlaylistAdmin;
-- 28
drop table Saved;
-- 27
drop table Follow;
-- 26
drop table PlaylistGenre;
-- 25
drop table PlaylistMood;
-- 24
drop table RadioMood;
-- 23
drop table RadioGenre;
-- 22
drop table ArtistGenre;
-- 21
drop table SongGenre;
-- 20
drop table AlbumGenre;
-- 19
drop table Mood;
-- 18
drop table Genre;
-- 17
drop table UserListen;
-- 16
drop table RadioSongs;
-- 15
drop table Radio;
-- 14
drop table PlaylistSongs;
-- 13
drop table Playlist;
-- 12
drop table SongListen;
-- 11
drop table Song;
-- 10
drop table AlbumArtist;
-- 9
drop table Artist;
-- 8
drop table Album;
-- 7
drop table Followable;
-- 6
drop table UserFollow;
-- 5
drop table Transaction;
-- 4
drop table UserDetails;
-- 3
DROP TABLE AbstractUser;
-- 2
DROP TABLE Country;
-- 1


