CREATE TABLE Album (
  album_name name_domain PRIMARY KEY ,
  year INTEGER,
  CHECK (year > 1900 and year < 2018),
  studio VARCHAR(20),
  total_plays INTEGER,
  CHECK (total_plays >= 0),
  total_downloads INTEGER,
  CHECK (total_downloads >= 0),
  cover_photo_path VARCHAR(100)
);
