CREATE TABLE Artist (
  artist_name name_domain PRIMARY KEY ,
  verified BOOLEAN NOT NULL DEFAULT FALSE ,
  f_id INTEGER REFERENCES followable(f_id),
  photo_path VARCHAR(100),
  UNIQUE (f_id)
);

CREATE TABLE AlbumArtist (
  artist_name name_domain REFERENCES Artist(artist_name),
  album_name name_domain REFERENCES Album(album_name),
  PRIMARY KEY (artist_name, album_name)
);