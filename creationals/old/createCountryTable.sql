CREATE TABLE Country (
  country_name VARCHAR(20) PRIMARY KEY ,
  continent VARCHAR(10) NOT NULL,
  CHECK (continent = 'asia' or continent = 'europe' or continent = 'africa' or continent = 'australia' or continent = 'america'),
  geo_location VARCHAR(20),
  flag_path VARCHAR(100),
  language VARCHAR(10) DEFAULT 'english',
  CHECK (language = 'english' or language = 'farsi')
);