CREATE TABLE followable(
  f_id BIGSERIAL PRIMARY KEY ,
  type  VARCHAR(10),
  CHECK (type = 'artist' or type = 'playlist' or type = 'radio')
);