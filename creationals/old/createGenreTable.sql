CREATE TABLE Genre(
  genre_name VARCHAR(20) PRIMARY KEY ,
  photo_path VARCHAR(100)
);

CREATE TABLE Mood(
  mood_name name_domain PRIMARY KEY ,
  photo_path VARCHAR(100)
);

CREATE TABLE AlbumGenre (
  album_name name_domain REFERENCES album(album_name),
  genre_name VARCHAR(20) REFERENCES genre(genre_name),
  PRIMARY KEY (album_name, genre_name)
);

CREATE TABLE SongGenre(
  album_name name_domain,
  song_name name_domain,
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name),
  genre_name VARCHAR(20) REFERENCES genre(genre_name),
  PRIMARY KEY (album_name,song_name, genre_name)
);

CREATE TABLE ArtistGenre (
  artist_name name_domain REFERENCES Artist(artist_name),
  genre_name VARCHAR(20) REFERENCES Genre(genre_name),
  PRIMARY KEY (artist_name, genre_name)
);

CREATE TABLE RadioGenre (
  radio_name name_domain REFERENCES Radio(radio_name),
  genre_name VARCHAR(20) REFERENCES Genre(genre_name),
  PRIMARY KEY (radio_name, genre_name)
);

CREATE TABLE RadioMood (
  radio_name name_domain REFERENCES Radio(radio_name),
  mood_name VARCHAR(20) REFERENCES Mood(mood_name),
  PRIMARY KEY (radio_name, mood_name)
);

CREATE TABLE PlaylistMood (
  playlist_name name_domain REFERENCES Playlist(playlist_name),
  mood_name VARCHAR(20) REFERENCES Mood(mood_name),
  PRIMARY KEY (playlist_name, mood_name)
)

