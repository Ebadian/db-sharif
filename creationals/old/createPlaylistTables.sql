CREATE TABLE Playlist (
  playlist_name name_domain PRIMARY KEY ,
  photo_path VARCHAR(100),
  f_id BIGINT REFERENCES followable(f_id),
  UNIQUE (f_id)
);

CREATE TABLE PlaylistSongs (
  playlist_name name_domain REFERENCES Playlist (playlist_name),
  album_name    name_domain,
  song_name     name_domain,
  added_time    TIMESTAMP NOT NULL DEFAULT now(),
  FOREIGN KEY (album_name, song_name) REFERENCES song (album_name, song_name),
  PRIMARY KEY (playlist_name, album_name, song_name)
);

CREATE TABLE UserListen (
  username name_domain REFERENCES abstractuser(username),
  album_name name_domain,
  song_name name_domain,
  day TIMESTAMP not NULL DEFAULT now(),
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name)
);
