CREATE TABLE Radio(
  radio_name name_domain PRIMARY KEY ,
  language VARCHAR(10),
  CHECK (language = 'english' or language = 'farsi'),
  photo_path VARCHAR(100),
  country_name VARCHAR(20) REFERENCES country(country_name),
  f_id BIGINT REFERENCES followable(f_id),
  UNIQUE (f_id)
);

CREATE TABLE RadioGenre(
  radio_name  name_domain REFERENCES radio(radio_name),
  genre_name name_domain REFERENCES genre(genre_name),
  PRIMARY KEY (radio_name, genre_name)
);

