CREATE TABLE Song(
  album_name name_domain REFERENCES album(album_name),
  song_name name_domain,
  total_plays INTEGER DEFAULT 0,
  CHECK (total_plays >= 0),
  total_downloads INTEGER DEFAULT 0,
  CHECK (total_downloads >= 0),
  song_path VARCHAR(100) UNIQUE ,
  PRIMARY KEY (album_name, song_name)
);

CREATE TABLE SongListen(
  album_name name_domain,
  song_name name_domain,
  day TIMESTAMP not NULL DEFAULT now(),
  FOREIGN KEY (album_name, song_name) REFERENCES Song(album_name, song_name)
);

