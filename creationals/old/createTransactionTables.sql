CREATE TABLE transaction (
  t_id VARCHAR(30) PRIMARY KEY ,
  gateway VARCHAR(20),
  CHECK (gateway = 'saman' or gateway = 'shaparak'),
  amount INTEGER NOT NULL,
  t_time TIMESTAMP NOT NULL ,
  account_number VARCHAR(20),
  username name_domain REFERENCES abstractuser(username)
);