CREATE TABLE AbstractUser (
  username name_domain PRIMARY KEY,
  email VARCHAR(100) not NULL UNIQUE,
  password VARCHAR(30) not NULL,
  isAdmin BOOLEAN not NULL DEFAULT FALSE
);

CREATE TABLE UserDetails (
  username                name_domain PRIMARY KEY,
  premium_expiration_date TIMESTAMP,
  postal_code             VARCHAR(30),
  view_details            INTEGER,
  CHECK (view_details = 1 OR view_details = 2 OR view_details = 3)
);

CREATE TABLE UserFollow (
  follower_username  name_domain REFERENCES AbstractUser(username),
  following_username name_domain REFERENCES AbstractUser(username),
  accepted BOOLEAN NOT NULL DEFAULT FALSE ,
  PRIMARY KEY (follower_username, following_username)
);

