-- 1
INSERT INTO Country (country_name, continent, language) VALUES
  ('iran', 'asia', 'farsi'),
  ('india', 'asia', 'english'),
  ('egypt', 'africa', 'english'),
  ('switzerland', 'europe', 'english');

-- 2
INSERT INTO AbstractUser (username, email, password, isAdmin) VALUES
  ('soroush', 'soroush@divar.ir', 'secret', TRUE ),
  ('parand', 'parand@cafebazaar.ir', 'secret2', TRUE );
INSERT INTO AbstractUser (username, email, password) VALUES
  ('ali', 'ali@divar.ir', 'secret' ),
  ('hassan', 'hassan@gmail.com', 'secret2' );

-- 3
INSERT INTO UserDetails (username, view_details, premium_expiration_date, country_name) VALUES
  ('soroush', 2, '2018/02/20', 'iran'),
  ('parand', 3, '2018/02/21', 'switzerland'),
  ('ali', 2, '1990/01/20', 'iran'),
  ('hassan', 1, '2010/02/20', 'india');

-- 4
-- INSERT INTO easytransaction (t_id, gateway, amount,  username) VALUES
--   ('190', 'saman', 19000, 'soroush'),
--   ('191', 'saman', 1000, 'soroush'),
--   ('192', 'shaparak', 10000, 'soroush'),
--   ('1010', 'saman', 500 , 'parand');

-- 5
INSERT INTO UserFollow (follower_username, following_username, accepted) VALUES
  ('soroush', 'parand', TRUE ),
  ('parand', 'soroush', TRUE ),
  ('soroush', 'hassan', NULL ),
  ('hassan', 'parand', FALSE ),
  ('hassan', 'ali', FALSE ),
  ('hassan', 'soroush', TRUE );

-- 6
INSERT INTO Followable (f_id, type) VALUES
  (1, 'artist'),
  (2, 'artist'),
  (3, 'playlist'),
  (4, 'playlist'),
  (5, 'radio'),
  (6, 'radio'),
  (7, 'radio'),
  (11, 'artist'),
  (12, 'artist'),
  (13, 'playlist'),
  (14, 'playlist');

-- 7
INSERT INTO Album (album_name, year) VALUES
  ('the wall', 1979),
  ('the division bell', 1994),
  ('animals', 1977),
  ('radio K.A.O.S', 1987),
  ('Amused to death', 1992),
  ('Kid A', 2000),
  ('a moon shaped pool', 2016);

-- 8
INSERT INTO Artist (artist_name, f_id) VALUES
  ('david gilmour', 1),
  ('roger waters', 2),
  ('pinkfloyd', 11),
  ('radiohead', 12);

-- 9
INSERT INTO AlbumArtist (album_name, artist_name) VALUES
  ('the wall', 'david gilmour'),
  ('the division bell', 'david gilmour'),
  ('animals', 'david gilmour'),
  ('the wall', 'roger waters'),
  ('the division bell', 'roger waters'),
  ('animals', 'roger waters'),
  ('the wall', 'pinkfloyd'),
  ('the division bell', 'pinkfloyd'),
  ('animals', 'pinkfloyd'),
  ('radio K.A.O.S', 'roger waters'),
  ('Amused to death', 'roger waters'),
  ('Kid A', 'radiohead'),
  ('a moon shaped pool', 'radiohead');

-- 10
INSERT INTO Song (album_name, song_name, song_path) VALUES
  ('the wall', 'in the flesh?', '1'),
  ('the wall', 'The Thin Ice', '2'),
  ('the wall', 'Another Brick in the Wall', '3'),
  ('the division bell', 'Cluster One', '4'),
  ('the division bell', 'What Do You Want from Me', '5'),
  ('the division bell', 'Poles Apart', '6'),
  ('the division bell', 'Marooned', '7'),
  ('animals', 'dogs', '8'),
  ('radio K.A.O.S', 'radio waves', '9'),
  ('radio K.A.O.S', 'me or him', '10'),
  ('Amused to death', 'amused to death', '11'),
  ('Amused to death', 'what god wants1', '12'),
  ('Amused to death', 'what god wants2', '13'),
  ('Amused to death', 'what god wants3', '14'),
  ('Kid A', 'kid a', '15'),
  ('a moon shaped pool', 'day dreaming', '16');

-- 11
INSERT INTO SongListen (album_name, song_name, count) VALUES
  ('the wall', 'Another Brick in the Wall', 3),
  ('the division bell', 'Cluster One', 3),
  ('the division bell', 'Marooned', 10),
  ('animals', 'dogs', 9),
  ('Amused to death', 'amused to death', 16),
  ('Amused to death', 'what god wants1', 15),
  ('a moon shaped pool', 'day dreaming', 3);

-- 12
INSERT INTO Playlist (playlist_name, f_id) VALUES
  ('rock coffee house', 3),
  ('progressive', 4),
  ('playlist1', 13),
  ('playlist2', 14);

-- 13
INSERT INTO PlaylistSongs (playlist_name, album_name, song_name) VALUES
  ('rock coffee house', 'the division bell', 'Marooned'),
  ('rock coffee house', 'the wall', 'in the flesh?'),
  ('rock coffee house', 'the division bell', 'Cluster One'),
  ('rock coffee house', 'radio K.A.O.S', 'me or him'),
  ('progressive', 'the wall', 'Another Brick in the Wall'),
  ('progressive', 'the division bell', 'Poles Apart'),
  ('progressive', 'animals', 'dogs'),
  ('playlist1', 'a moon shaped pool', 'day dreaming'),
  ('playlist1', 'Kid A', 'kid a'),
  ('playlist1', 'the division bell', 'What Do You Want from Me'),
  ('playlist1', 'Amused to death', 'what god wants3'),
  ('playlist2', 'the wall', 'The Thin Ice');

-- 14
INSERT INTO Radio(radio_name, country_name, f_id) VALUES
  ('80s rock', 'egypt', 5),
  ('70s rock', 'iran', 6),
  ('90s rock', 'india', 7);

-- 15
INSERT INTO RadioSongs(radio_name, album_name, song_name) VALUES
  ('70s rock', 'the wall', 'in the flesh?'),
  ('70s rock', 'the wall', 'The Thin Ice'),
  ('70s rock', 'the wall', 'Another Brick in the Wall'),
  ('90s rock', 'the division bell', 'Cluster One'),
  ('90s rock', 'the division bell', 'What Do You Want from Me'),
  ('70s rock', 'animals', 'dogs'),
  ('80s rock', 'radio K.A.O.S', 'radio waves'),
  ('80s rock', 'radio K.A.O.S', 'me or him'),
  ('90s rock', 'Amused to death', 'amused to death'),
  ('90s rock', 'Amused to death', 'what god wants3');

-- 16
INSERT INTO UserListen (username, album_name, song_name) VALUES
  ('soroush', 'the wall', 'in the flesh?'),
  ('parand', 'the wall', 'in the flesh?'),
  ('hassan', 'the wall', 'in the flesh?'),
  ('ali', 'the wall', 'in the flesh?'),
  ('soroush', 'the wall', 'The Thin Ice'),
  ('soroush', 'the wall', 'in the flesh?'),
  ('soroush', 'the wall', 'Another Brick in the Wall'),
  ('parand', 'the division bell', 'Cluster One'),
  ('hassan', 'Amused to death', 'amused to death');

-- 17
INSERT INTO Genre (genre_name) VALUES
  ('rock'),
  ('progressive rock'),
  ('classical');

-- 18
INSERT INTO Mood (mood_name) VALUES
  ('happy'),
  ('sad'),
  ('rainy'),
  ('summer');

-- 19
INSERT INTO AlbumGenre (album_name, genre_name) VALUES
  ('the wall', 'rock'),
  ('the wall', 'progressive rock'),
  ('the division bell', 'classical'),
  ('the division bell', 'rock'),
  ('animals', 'classical'),
  ('Kid A', 'progressive rock'),
  ('Kid A', 'rock'),
  ('Kid A', 'classical'),
  ('a moon shaped pool', 'progressive rock');

-- 20
INSERT INTO SongGenre (album_name, song_name, genre_name) VALUES
  ('the wall', 'in the flesh?', 'rock'),
  ('the wall', 'The Thin Ice', 'rock'),
  ('the wall', 'Another Brick in the Wall', 'classical'),
  ('the division bell', 'Poles Apart', 'rock'),
  ('the division bell', 'Marooned', 'rock'),
  ('animals', 'dogs', 'rock'),
  ('radio K.A.O.S', 'radio waves', 'progressive rock'),
  ('radio K.A.O.S', 'me or him', 'progressive rock'),
  ('Amused to death', 'what god wants3', 'classical'),
  ('Kid A', 'kid a', 'progressive rock'),
  ('a moon shaped pool', 'day dreaming', 'progressive rock'),
  ('a moon shaped pool', 'day dreaming', 'rock');

-- 21
INSERT INTO ArtistGenre (artist_name, genre_name) VALUES
  ('david gilmour', 'rock'),
  ('roger waters', 'rock'),
  ('pinkfloyd', 'rock'),
  ('radiohead', 'progressive rock');

-- 22
INSERT INTO RadioGenre (radio_name, genre_name) VALUES
  ('80s rock', 'rock'),
  ('70s rock', 'rock'),
  ('90s rock', 'rock'),
  ('90s rock', 'progressive rock');

-- 23
INSERT INTO RadioMood (radio_name, mood_name) VALUES
  ('80s rock', 'sad'),
  ('80s rock', 'happy'),
  ('80s rock', 'rainy'),
  ('70s rock', 'happy'),
  ('90s rock', 'sad'),
  ('90s rock', 'rainy');

-- 24
INSERT INTO PlaylistMood (playlist_name, mood_name) VALUES
  ('rock coffee house', 'happy'),
  ('progressive', 'sad'),
  ('progressive', 'rainy'),
  ('playlist1', 'happy'),
  ('playlist1', 'sad'),
  ('playlist1', 'rainy'),
  ('playlist2', 'sad');

-- 25
INSERT INTO PlaylistGenre (playlist_name, genre_name) VALUES
  ('rock coffee house', 'rock'),
  ('rock coffee house', 'classical'),
  ('progressive', 'rock'),
  ('progressive', 'progressive rock'),
  ('playlist1', 'classical'),
  ('playlist2', 'rock');

-- 26
INSERT INTO Follow (username, f_id) VALUES
  ('soroush', 1),
  ('soroush', 2),
  ('soroush', 3),
  ('soroush', 4),
  ('soroush', 5),
  ('soroush', 6),
  ('soroush', 7),
  ('soroush', 11),
  ('soroush', 12),
  ('soroush', 13),
  ('soroush', 14),
  ('parand', 1),
  ('parand', 6),
  ('parand', 7),
  ('parand', 13),
  ('parand', 14),
  ('hassan', 13),
  ('hassan', 14);

-- 27
INSERT INTO Saved (album_name, song_name, username) VALUES
  ('the wall', 'in the flesh?', 'soroush'),
  ('the wall', 'The Thin Ice', 'soroush'),
  ('the wall', 'Another Brick in the Wall', 'parand'),
  ('the division bell', 'Cluster One', 'parand'),
  ('the division bell', 'What Do You Want from Me', 'parand'),
  ('the division bell', 'Poles Apart', 'parand'),
  ('the division bell', 'Marooned', 'parand'),
  ('animals', 'dogs', 'hassan'),
  ('radio K.A.O.S', 'radio waves', 'hassan'),
  ('radio K.A.O.S', 'me or him', 'hassan'),
  ('Amused to death', 'amused to death', 'hassan'),
  ('Amused to death', 'what god wants1', 'hassan'),
  ('Amused to death', 'what god wants2', 'ali'),
  ('Amused to death', 'what god wants3', 'hassan'),
  ('Kid A', 'kid a', 'hassan'),
  ('a moon shaped pool', 'day dreaming', 'hassan'),
  ('the wall', 'in the flesh?', 'hassan'),
  ('the wall', 'The Thin Ice', 'hassan'),
  ('the wall', 'Another Brick in the Wall', 'hassan'),
  ('the division bell', 'Cluster One', 'hassan'),
  ('the division bell', 'What Do You Want from Me', 'hassan'),
  ('the division bell', 'Poles Apart', 'hassan'),
  ('the division bell', 'Marooned', 'ali');


-- 28
INSERT INTO PlaylistAdmin (playlist_name, username) VALUES
  ('rock coffee house', 'soroush'),
  ('progressive', 'parand'),
  ('progressive', 'soroush'),
  ('playlist1', 'soroush'),
  ('playlist1', 'hassan'),
  ('playlist1', 'ali'),
  ('playlist2', 'hassan');
