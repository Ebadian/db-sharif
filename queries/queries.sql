

select * from song;
SELECT * from songlisten;

UPDATE song set total_plays = 5 where song_name = 'me or him';

--most listened song
 select * from songlisten as A
 where (
   (
     SELECT sum(count) from songlisten as D
     GROUP BY song_name, album_name
     HAVING ( A.song_name = D.song_name and A.album_name = D.album_name)
   ) >= All (
     SELECT sum(count) from songlisten as B
     GROUP BY B.song_name, B.album_name
   )
 );

--most downloaded song
select album_name, song_name, count(*) from saved as A
where (
  (
    SELECT count(*) from saved as D
    GROUP BY D.song_name, D.album_name
    having ( A.song_name = D.song_name and A.album_name = D.album_name)
  ) >= All (
    SELECT count(*) from saved as B
    GROUP BY B.song_name, B.album_name
  )
)
GROUP BY A.album_name, A.song_name;

-- top 10 most followed artists
SELECT artist_name, count(*) from (
    artist join follow on follow.f_id = artist.f_id)
GROUP BY artist_name
order BY count(*) desc
limit 10;

-- top 10 most followed playlists
SELECT playlist_name, count(*) from (
    playlist join follow on follow.f_id = playlist.f_id)
GROUP BY playlist_name
order BY count(*) desc
limit 10;

-- top 10 most followed radios
SELECT radio_name, count(*) from (
    radio join follow on follow.f_id = radio.f_id)
GROUP BY radio_name
order BY count(*) desc
limit 10;