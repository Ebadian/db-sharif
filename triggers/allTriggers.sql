
-----1
CREATE FUNCTION f() RETURNS TRIGGER AS $songTotalDownloads$
BEGIN
  IF new.total_downloads < old.total_downloads THEN
    RAISE EXCEPTION 'this is illegal';
  END IF;
  RETURN new;
END;
$songTotalDownloads$ LANGUAGE plpgsql;

create TRIGGER songDownloads BEFORE UPDATE ON song
  for EACH row EXECUTE PROCEDURE f();

-----2

CREATE FUNCTION f1() RETURNS TRIGGER AS $songPlays$
BEGIN
  IF new.total_plays < old.total_plays THEN
    RAISE EXCEPTION 'this is illegal';
  END IF;
  RETURN new;
END;
$songPlays$ LANGUAGE plpgsql;

CREATE TRIGGER songPlays BEFORE UPDATE ON song
  FOR EACH ROW EXECUTE PROCEDURE f1();

-----3


CREATE FUNCTION f2() RETURNS TRIGGER AS $songListenCount$
BEGIN
  IF new.count < old.count THEN
    RAISE EXCEPTION 'this is illegal';
  END IF;
  RETURN new;
END;
$songListenCount$ LANGUAGE plpgsql;

CREATE TRIGGER songListenCount BEFORE UPDATE on songlisten
  FOR EACH ROW EXECUTE PROCEDURE f2();


-----4

CREATE FUNCTION f3() RETURNS TRIGGER AS $songListenDay$
BEGIN
  IF new.day < old.day THEN
    RAISE EXCEPTION 'this is illegal';
  END IF;
  RETURN new;
END;
$songListenDay$ LANGUAGE plpgsql;

CREATE TRIGGER songListenDay BEFORE UPDATE on songlisten
  FOR EACH ROW EXECUTE PROCEDURE f3();

-----5
drop TRIGGER addOnEasyTransaction on easytransaction;

CREATE FUNCTION f4() RETURNS TRIGGER AS $insertView$
BEGIN
  INSERT INTO transaction VALUES (new.t_id, new.gateway, new.amount, now(), new.account_number, new.username);
  return new;
END;
$insertView$ LANGUAGE plpgsql;

CREATE TRIGGER addOnEasyTransaction INSTEAD OF INSERT ON easytransaction
  FOR EACH ROW EXECUTE PROCEDURE f4();
