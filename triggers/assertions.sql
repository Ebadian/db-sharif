-----1
CREATE assertion totalListens
check not exist (select * from song where song.total_listens < (select sum(count) from songListen group by (album_name, song_name) having album_name = song.album_name and song.song_name = song_name));
-----2
CREATE assertion f_idArtist
check not exist (select * from followable where type = 'artist'and not exist (select * from artist where artist.f_id = followable.f_id));
---3
CREATE assertion f_idPlaylist
check not exist (select * from followable where type = 'playlist'and not exist (select * from playlist where playlist.f_id = followable.f_id));
---4
CREATE assertion f_idRadio
check not exist (select * from followable where type = 'radio'and not exist (select * from radio where radio.f_id = followable.f_id));
