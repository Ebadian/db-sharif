-- all site users
CREATE OR REPLACE VIEW AllUsers AS
  Select abstractuser.email, userdetails.*
  from abstractuser NATURAL JOIN userdetails;

-- admins
CREATE OR REPLACE VIEW AdminUsers AS
  SELECT username, email FROM abstractuser where isadmin = TRUE;

-- following artists
CREATE OR REPLACE VIEW UserArtistFollows AS
  SELECT follow.username, artist.*
  FROM follow NATURAL JOIN artist;

-- following people

CREATE OR REPLACE VIEW UserUserAcceptedFollow AS
  SELECT F.follower_username,
    A.email as follower_email,
    F.following_username,
    B.email as following_email
  FROM userfollow as F
    INNER JOIN abstractuser as A on F.follower_username = A.username
    INNER JOIN abstractuser as B on F.following_username = B.username
  WHERE F.accepted = TRUE ;


-- view album/artist/playlist




--view on transactions

CREATE or REPLACE VIEW easyTransaction AS
  SELECT t_id, gateway, amount, account_number, username from transaction;